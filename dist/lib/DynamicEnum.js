'use strict';

var _get = require('babel-runtime/helpers/get')['default'];

var _inherits = require('babel-runtime/helpers/inherits')['default'];

var _createClass = require('babel-runtime/helpers/create-class')['default'];

var _classCallCheck = require('babel-runtime/helpers/class-call-check')['default'];

var _interopRequireDefault = require('babel-runtime/helpers/interop-require-default')['default'];

Object.defineProperty(exports, '__esModule', {
  value: true
});

var _Keyword2 = require('./Keyword');

var _Keyword3 = _interopRequireDefault(_Keyword2);

var DynamicEnum = (function (_Keyword) {
  _inherits(DynamicEnum, _Keyword);

  function DynamicEnum(value) {
    _classCallCheck(this, DynamicEnum);

    _get(Object.getPrototypeOf(DynamicEnum.prototype), 'constructor', this).call(this);
    this.value = value;
  }

  _createClass(DynamicEnum, [{
    key: 'json',
    value: function json(context) {
      context = context || {};
      context.dynamicEnum = this.value;
      return context;
    }
  }, {
    key: 'value',
    get: function get() {
      return this._value;
    },
    set: function set(value) {
      if (typeof value != 'string') {
        // TODO better validation
        throw new Error('value must be a valid string');
      }

      this._value = value;
    }
  }]);

  return DynamicEnum;
})(_Keyword3['default']);

exports['default'] = DynamicEnum;
module.exports = exports['default'];
//# sourceMappingURL=DynamicEnum.js.map
